FROM registry.gitlab.com/dedyms/buster-slim:node16-dev AS tukang
RUN apt update && \
    apt install -y --no-install-recommends python3-pip python3-setuptools python3-dev grc git build-essential
USER $CONTAINERUSER
RUN git clone --recursive https://github.com/Unmanic/unmanic.git $HOME/unmanic #app clone
#RUN git clone https://github.com/Unmanic/unmanic-frontend.git $HOME/unmanic/unmanic/webserver/frontend #frontend clone
WORKDIR $HOME/unmanic
RUN sed -i 's/isAlive/is_alive/g' unmanic/libs/foreman.py
RUN npm install @quasar/cli && \
    pip3 install --no-cache-dir -r requirements.txt && \
    python3 ./setup.py install --user

FROM registry.gitlab.com/dedyms/unmanic:base
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/.local/ $HOME/.local/
#COPY --from=ffmpeg --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/.local/bin/ $HOME/.local/bin/
USER $CONTAINERUSER
RUN mkdir $HOME/.unmanic /tmp/unmanic
WORKDIR $HOME/.unmanic
VOLUME $HOME/.unmanic
VOLUME /tmp/unmanic
CMD ["bash", "-c", "unmanic --dev"]
